## Tony Morris { data-transition="fade none" }

I work full-time as a software engineer, and casually as a flight instructor at Archerfield Airport.

I am the Course Coordinator for COMP3400. I will also be lecturing the course
and running most of the practicals.

[t.morris@uq.edu.au](mailto:t.morris@uq.edu.au)

---

