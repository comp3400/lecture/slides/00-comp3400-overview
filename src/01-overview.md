## Overview { data-transition="fade none" }

This course will emphasise ideas such as abstraction and program correctness, by utilising tools and concepts that are unavailable in other programming disciplines.

---

