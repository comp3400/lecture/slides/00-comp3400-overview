## Brian McKenna { data-transition="fade none" }

Brian is a Principal Developer at Atlassian and has been teaching FP to his
colleagues for the past few years.

Brian will be assisting COMP3400 in creating course material and answering
technical questions.

[brian.mckenna@uq.edu.au](mailto:brian.mckenna@uq.edu.au)

---

